/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const config = require('./webpack.config')

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig(config)
}
