export const hero: string
export const left: string
export const right: string
export const heroTextContainer: string
export const greetingWrapper: string
export const greeting: string
export const info: string
