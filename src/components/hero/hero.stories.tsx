import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs'

import Hero from './hero'

storiesOf(`components.Hero`, module)
  .addDecorator(withKnobs)
  .add(`default`, () => (
    <>
      <Hero
        greeting={text('greeting', "Hi! I'm Paweł")}
        info={text('info', '< Fullstack Web Engineer />')}
      />
      <div style={{ height: '1200px' }}>f</div>
    </>
  ))
