import React from 'react'
import { storiesOf } from '@storybook/react'
import Layout from './layout'

const seoData = {
  site: {
    siteMetadata: {
      title: 'Default title',
      description: 'Default description',
      author: 'Default author',
    },
  },
}

storiesOf(`components.Layout`, module).add(`default`, () => (
  <Layout __seoData={seoData} />
))
