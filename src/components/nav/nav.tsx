import React from 'react'
import logo from '@/images/base-light-horizontal.svg'
import style from './nav.module.css'

export const Nav: React.FC<{}> = () => (
  <nav className={style.nav}>
    <img alt="logo" src={logo} className={style.logo} />
  </nav>
)
export default Nav
