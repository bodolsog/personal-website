// For compatibility with PyCharm
var path = require('path')
const { TypedCssModulesPlugin } = require('typed-css-modules-webpack-plugin')

module.exports = {
  resolve: {
    modules: ['node_modules'],
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
  },
  plugins: [
    new TypedCssModulesPlugin({
      globPattern: 'src/**/*.css',
    }),
  ],
}
